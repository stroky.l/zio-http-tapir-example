import sbt._

object Libraries {
  val slf4jVersion = "1.7.36"
  val zioVersion = "2.0.10"
  val zioJsonVersion = "0.5.0"
  val zioHttpVersion = "0.0.5"
  val quillZioVersion = "4.6.0.1"
  val tapirVersion = "1.2.11"

  val loggingDeps = Seq(
    "ch.qos.logback" % "logback-classic" % "1.2.11",
    "org.slf4j" % "jul-to-slf4j" % slf4jVersion,
    "org.slf4j" % "log4j-over-slf4j" % slf4jVersion,
    "org.slf4j" % "jcl-over-slf4j" % slf4jVersion,
  )

  val effectDeps = Seq(
    "dev.zio" %% "zio" % zioVersion,
    "dev.zio" %% "zio-json" % zioJsonVersion,
  )

  val databaseDeps = Seq(
    "io.getquill" %% "quill-zio" % quillZioVersion,
    "io.getquill" %% "quill-jdbc-zio" % quillZioVersion,
    "com.h2database" % "h2" % "2.1.214"
  )

  val apiDeps = Seq(
    "dev.zio" %% "zio-http" % zioHttpVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-core" % tapirVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-openapi-docs" % tapirVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-swagger-ui-bundle" % tapirVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-zio-http-server" % tapirVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-json-zio" % tapirVersion,
  )

  val commonDeps = loggingDeps ++ effectDeps ++ databaseDeps ++ apiDeps
}