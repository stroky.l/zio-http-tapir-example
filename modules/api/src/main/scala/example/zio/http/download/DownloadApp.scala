package example.zio.http.download

import zio.http.*
import zio.*
import zio.http.model.{Header, Headers, Method}
import zio.stream.ZStream

/**
 * An http app that: 
 *   - Accepts a `Request` and returns a `Response` 
 *   - May fail with type of `Throwable`
 *   - Does not require any environment
 */
object DownloadApp:
  def apply(): Http[Any, Throwable, Request, Response] =
    Http.collect[Request] {
      // GET /download
      case Method.GET -> !! / "download" =>
        val fileName = "file.txt"
        val body = Body.fromStream(ZStream.fromResource(fileName))
        Response(body = body).setHeaders(
          Headers(
            Header("Content-Type", "application/octet-stream"),
            Header("Content-Disposition", s"attachment; filename=${fileName}")
          )
        )

      // Download a large file using streams
      // GET /download/stream
      case Method.GET -> !! / "download" / "stream" =>
        val file = "bigfile.txt"
        val body = Body.fromStream(ZStream.fromResource(file)
          .schedule(Schedule.spaced(50.millis)))
        Response(body = body).setHeaders(
          Headers(
            Header("Content-Type", "application/octet-stream"),
            Header("Content-Disposition", s"attachment; filename=${file}")
          )
        )
    }
