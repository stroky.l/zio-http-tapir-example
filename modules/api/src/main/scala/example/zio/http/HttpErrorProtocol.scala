package example.zio.http

import zio.http.model.HttpError.*
import zio.json.*

trait HttpErrorProtocol {
  given JsonDecoder[Conflict] = DeriveJsonDecoder.gen[Conflict]
  given JsonEncoder[Conflict] = DeriveJsonEncoder.gen[Conflict]
  given JsonDecoder[NotFound] = DeriveJsonDecoder.gen[NotFound]
  given JsonEncoder[NotFound] = DeriveJsonEncoder.gen[NotFound]
}

object HttpErrorProtocol extends HttpErrorProtocol