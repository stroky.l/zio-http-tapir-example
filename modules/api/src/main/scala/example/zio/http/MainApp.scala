package example.zio.http

import example.zio.http.counter.CounterApp
import example.zio.http.download.DownloadApp
import example.zio.http.greet.GreetingApp
import example.zio.http.users.{InmemoryUserRepo, PersistentUserRepo, UserApp, UserAppTapir, UserRepo}
import sttp.tapir.server.ziohttp.ZioHttpInterpreter
import sttp.tapir.swagger.bundle.SwaggerInterpreter
import sttp.tapir.ztapir.ZServerEndpoint
import zio.*
import zio.http.*

object MainApp extends ZIOAppDefault:
  val swaggerEndpoints: List[ZServerEndpoint[Any, Any]] = SwaggerInterpreter().fromEndpoints(UserAppTapir.endpoints, "Example app", "0.1.0-SNAPSHOT")

  val swaggerRoutes: HttpApp[Any, Throwable] = ZioHttpInterpreter().toHttp(swaggerEndpoints)

  val app: HttpApp[UserRepo & Ref[Int], Throwable] = GreetingApp() ++ UserApp() ++ CounterApp() ++ DownloadApp() ++ UserAppTapir() ++ swaggerRoutes

  def run: ZIO[Environment with ZIOAppArgs with Scope,Any,Any] =
    Server.serve(app.withDefaultErrorResponse).provide(
      // An layer responsible for storing the state of the `counterApp`
      ZLayer.fromZIO(Ref.make(42)),

      // To use the persistence layer, provide the `PersistentUserRepo.layer` layer instead
      InmemoryUserRepo.layer,
      Server.defaultWithPort(3232)
    )
