package example.zio.http.counter

import zio.http.*
import zio.http.model.Method
import zio.{Ref, ZIO}

/**
 * An http app that:  
 *  - Accepts `Request` and returns a `Response`
 *  - Does not fail
 *  - Requires the `Ref[Int]` as the environment
 */
object CounterApp:
  val ref = ZIO.serviceWithZIO[Ref[Int]]
  def updateAndGet(f: Int => Int) = ref(_.updateAndGet(f))
  def get = ref(_.get)

  def apply(): Http[Ref[Int], Nothing, Request, Response] = {
    Http.collectZIO[Request] {
      case Method.GET -> !! / "up" =>
        updateAndGet(_ + 1)
          .map(_.toString).map(Response.text)
      case Method.GET -> !! / "down" =>
        updateAndGet(_ - 1)
          .map(_.toString).map(Response.text)
      case Method.GET -> !! / "get" =>
        get.map(_.toString).map(Response.text)
    }
  }
