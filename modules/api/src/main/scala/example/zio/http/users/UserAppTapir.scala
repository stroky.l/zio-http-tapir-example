package example.zio.http.users

import example.zio.http.HttpErrorProtocol
import sttp.model.StatusCode
import sttp.tapir.{AnyEndpoint, PublicEndpoint}
import sttp.tapir.generic.auto.*
import sttp.tapir.json.zio.*
import sttp.tapir.server.ziohttp.ZioHttpInterpreter
import sttp.tapir.ztapir.{endpoint as tapirEndpoint, *}
import zio.*
import zio.http.model.HttpError.*
import zio.http.model.{HttpError, Method, Status}
import zio.http.{endpoint, *}
import zio.json.*

import java.nio.charset.StandardCharsets

/**
 * An http app that: 
 *   - Accepts a `Request` and returns a `Response`
 *   - May fail with type of `Throwable`
 *   - Uses a `UserRepo` as the environment
 */
object UserAppTapir extends HttpErrorProtocol:
  def postUserLogic(user: User): ZIO[UserRepo, HttpError, String] =
    UserRepo.register(user)
      .mapError(throwable => InternalServerError(cause = Some(throwable)))

  val postUserEndpoint: PublicEndpoint[User, HttpError, String, Any] =
    tapirEndpoint
      .errorOut(
        oneOf[HttpError](
          oneOfVariant[Conflict](StatusCode.Conflict, jsonBody[Conflict])
        )
      )
      .post
      .in("tapir")
      .in("users")
      .in(jsonBody[User].example(User("Sarah", 18)))
      .out(plainBody[String])

  val postUserHttp: Http[UserRepo, Throwable, Request, Response] =
    ZioHttpInterpreter().toHttp(postUserEndpoint.zServerLogic[UserRepo](postUserLogic))

  def getUserLogic(id: String): ZIO[UserRepo, HttpError, User] =
    UserRepo.lookup(id)
      .foldZIO(
        failure => ZIO.fail(InternalServerError(cause = Some(failure))),
        success => success match {
          case Some(user) => ZIO.succeed(user)
          case None => ZIO.fail(NotFound(f"user $id not found"))
        }
      )

  val getUserEndpoint: PublicEndpoint[String, HttpError, User, Any] =
    tapirEndpoint
      .errorOut(
        oneOf[HttpError](
          oneOfVariant[NotFound](StatusCode.NotFound, jsonBody[NotFound])
        )
      )
      .get
      .in("tapir")
      .in("users")
      .in(path[String]("id"))
      .out(jsonBody[User])

  val getUserHttp: Http[UserRepo, Throwable, Request, Response] =
    ZioHttpInterpreter().toHttp(getUserEndpoint.zServerLogic[UserRepo](getUserLogic))

  val getUsersLogic: ZIO[UserRepo, HttpError, List[User]] =
    UserRepo.users
      .mapError(throwable => InternalServerError(cause = Some(throwable)))

  val getUsersEndpoint: PublicEndpoint[Unit, HttpError, List[User], Any] =
    tapirEndpoint
      .errorOut(
        oneOf[HttpError](
          oneOfVariant[NotFound](StatusCode.NotFound, jsonBody[NotFound])
        )
      )
      .in("tapir")
      .in("users")
      .get
      .out(jsonBody[List[User]])

  val getUsersHttp: Http[UserRepo, Throwable, Request, Response] =
    ZioHttpInterpreter().toHttp(getUsersEndpoint.zServerLogic[UserRepo](Unit => getUsersLogic))

  val endpoints: List[AnyEndpoint] = List(
    postUserEndpoint,
    getUserEndpoint,
    getUsersEndpoint
  )

  def apply(): Http[UserRepo, Throwable, Request, Response] =
    postUserHttp ++ getUserHttp ++ getUsersHttp
