import BuildHelper._
import Libraries._

ThisBuild / scalaVersion := "3.2.2"
ThisBuild / organization := "com.joli-ciel"
ThisBuild / homepage     := Some(url("https://www.joli-ciel.com/"))
ThisBuild / name         := "zio-http-tapir-example"
ThisBuild / licenses     := List("Apache-2.0" -> url("https://www.apache.org/licenses/LICENSE-2.0"))

lazy val zioHttpExampleVersion = sys.env.get("ZIO_HTTP_TAPIR_EXAMPLE_VERSION")
  .getOrElse{
    ConsoleLogger().warn("ZIO_HTTP_EXAMPLE_VERSION env var not found")
    "0.0.1-SNAPSHOT"
  }

val projectSettings = commonSettings ++ Seq(
  version := zioHttpExampleVersion,
)

lazy val root =
  Project(id = "zio-http-tapir-example", base = file("."))
    .settings(noDoc: _*)
    .aggregate(api)

lazy val api = project
  .in(file("modules/api"))
  .enablePlugins(JavaServerAppPackaging, DockerPlugin)
  .settings(projectSettings: _*)
  .settings(
    libraryDependencies ++= commonDeps,
    Docker / packageName := "zio-http-tapir-example",
    Docker / maintainer  := "Joliciel Informatique SARL",
    Docker / daemonUserUid := Some("1001"),
    dockerBaseImage := "openjdk:17.0.2-bullseye",
    Docker / dockerRepository := sys.env.get("DOCKER_REGISTRY"),
    Docker / version     := version.value,
    dockerExposedPorts := Seq(3232),
    dockerExposedVolumes := Seq("/opt/docker/index"),
    //do not package scaladoc
    Compile/ packageDoc / mappings := Seq(),
    Compile / mainClass := Some("example.zio.http.MainApp"),
    fork := true,
  )